<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("email")->nullable();
            $table->string('phone', 30)->unique();
            $table->string("password");
            $table->tinyInteger('status')->default(0);
            $table->text("token")->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_otps', function (Blueprint $table) {
            $table->id();
            $table->string('phone', 30)->unique();
            $table->integer('otp', 6);
            $table->dateTime('expired');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_otps');
    }
}
