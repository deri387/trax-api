<?php

namespace Database\Seeders;

use App\Utils\Helper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            [
                "name" => "Administrator",
                "phone" => "081902420670",
                "email" => "demo@gmail.com",
                "password" => bcrypt('demo'),
                "status" => 1,
            ],
        ]);
    }
}
