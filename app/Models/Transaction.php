<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Product;
use App\Models\User;

class Transaction extends Model
{
    use HasFactory;
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = \Carbon\Carbon::now();
        });
    }
    protected $appends = ["product", "user"];
    public function getProductAttribute() {
        return Product::where('id', $this->attributes['product_id'])->first();
    }

    public function getUserAttribute() {
        return User::where('id', $this->attributes['user_id'])->first();
    }
}
