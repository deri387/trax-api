<?php
namespace App\Utils;

use Illuminate\Support\Carbon;

class Helper {
    public static function randString($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateTransactionNumber()
    {
        $date = Carbon::now();
        $format = $date->isoFormat('x');
        return "TRAX-${format}";
    }

    public static function formatRupiah($angka)
    {
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
}