<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\UserOtp;
use App\Utils\Helper;

class AuthController extends Controller
{
    public function authenticatedAdministrator(Request $request) 
    {
        DB::beginTransaction();
        try {
            $credentials = ['email'=>$request->get('email'),'password'=>$request->get('password')];

            if(!Auth::attempt($credentials)) {
                DB::commit();
                return response()->json([
                    'message' => 'Email atau password kurang tepat.',
                    'serve' => []
                ], 400);
            }
            
            $user = Auth::user();
            \App\Models\OauthAccessToken::where('user_id', $user->user_id)->delete();
            $now = \Carbon\Carbon::now();

            $tokenResult = $user->createToken('token-'.$now.'-'.$user->email);
            $token = $tokenResult->token;   
            $token->save();
            DB::commit();
            return response()->json([
                'message' => '',
                'serve' => [
                    'access_token' => $tokenResult->accessToken,
                    'user' => $user
                ]
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function authenticated(Request $request) 
    {
        DB::beginTransaction();
        try {
            $validation = Validator::make($request->all(), [
                'phone' => 'required|string',
                'otp'   => 'required|string',
            ]);
            if ($validation->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validation->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataOtp = UserOTP::where('phone', $request->phone)->first();
        
            if (!$dataOtp) {
                DB::commit();
                return response()->json([
                    'message' => "Kode OTP tidak valid.",
                    'serve' => []
                ], 400);
            }

            if (\Carbon\Carbon::now()->gte(new \Carbon\Carbon($dataOtp->expired))) {
                $dataOtp->delete();
                DB::commit();
                return response()->json([
                    'message' => "Kode OTP kadaluwarsa atau tidak valid.",
                    'serve' => []
                ], 400);
            }

            if ($dataOtp->otp !== $request['otp']) {
                DB::commit();
                return response()->json([
                    'message' => "Kode OTP tidak valid.",
                    'serve' => []
                ], 400);
            }

            $dataOtp->delete();
            
            $dataUser = User::firstOrCreate(
                ["phone" => $request->phone],
                [
                    "name"     => "User".Helper::randString(),
                    "password" => Hash::make("Trax@123"),
                    "status"   => 1,
                    "token"    => $request->token,
                ],
            );

            UserBalance::firstOrCreate(
                ["user_id" => $dataUser->id],
                [
                    "wallet_address" => "5417".$dataUser->phone,
                ],
            );

            $now = \Carbon\Carbon::now();

            $tokenResult = $dataUser->createToken('token-'.$now.'-'.$dataUser->phone);
            $token = $tokenResult->token;   
            $token->save();

            DB::commit();
            return response()->json([
                'message' => "Selamat datang di Trax, tempat topup DM paling terpcaya.",
                'serve' => [
                    'access_token' => $tokenResult->accessToken,
                    'user' => $dataUser
                ]
            ], 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }

    public function sendOtp(Request $request) 
    {
        DB::beginTransaction();
        try {
            $validation = Validator::make($request->all(), [
                'phone' => 'required|string',
            ]);
            if ($validation->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validation->errors()->first(),
                    'serve' => []
                ], 400);
            }
            
            $dataOtp = UserOtp::where("phone", $request->phone)->get();
            if (count($dataOtp) > 0) {
                foreach($dataOtp as $otp) {
                    $otp->delete();
                }
            }

            $dataOtp = new UserOTP();
            $dataOtp->phone = $request->phone;
            $dataOtp->otp = Helper::randString(6);
            $dataOtp->expired = \Carbon\Carbon::now()->addMinutes(1);
            $dataOtp->save(); 

            $client = new Client();
            $client->post(env('APP_MIDDLEWARE')."/sendText", [
                'form_params' => [
                    'key' => env('OTP_KEY'),
                    'phone' => $request->phone,
                    'message' => 'JANGAN BERI kode ini ke siapa pun, TERMASUK TIM TRAX WASPADA PENIPUAN, masukan kode verifikasi (OTP) '.$dataOtp->otp
                ]
            ]);

            DB::commit();
            return response()->json([
                'message' => "OTP berhasil terkirim.",
                'serve' => []
            ], 200);
        } catch (\Throwable $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->user()->tokens->each(function ($token, $key) {
                $token->delete();
            });
            DB::commit();
            return response()->json([
                'message' => '',
            ], 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
    public function updatePassword(Request $request) 
    {
        DB::beginTransaction();
        try {     
            $validation = Validator::make($request->all(), [
                'password' => 'required|string|min:8|confirmed',
            ]);

            if ($validation->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validation->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataUser = User::where('id', Auth::user()->id)->first();
            if (!$dataUser) {
                DB::commit();
                return response()->json([
                    'message' => 'User tidak diketahui.',
                    'serve' => [],
                ], 400);
            }
            $dataUser->password = bcrypt($request->password);
            $dataUser->save();
            
            $success = [
                'message' => "Password berhasil diganti.",
                'serve' => []
            ];
            DB::commit();
            return response()->json($success, 200);
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
