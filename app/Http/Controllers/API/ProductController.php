<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function get(Request $request)
    {
        try {
            $name = $request->query("name");
            $sortBy = $request->query("sort_by");
            $sortByValue = $request->query("sort_by_value");

            $dataQuery = Product::when($name, function($query) use ($name){
                return $query->where('name', 'like', '%'.$name.'%');
            })->when($sortBy && $sortByValue, function($query) use ($sortBy, $sortByValue){
                return $query->orderBy($sortBy, $sortByValue);
            })->orderBy("created_at", "desc")
            ->paginate(10);
            return response()->json([
                'message' => '',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataQuery = new Product();
            $dataQuery->name = $request->name;
            $dataQuery->stock = $request->stock;
            $dataQuery->price = $request->price;
            $dataQuery->price_disc = $request->price_disc;
            $dataQuery->price_total = $request->price - ($request->price * ($request->price_disc / 100));
            $dataQuery->save();

            DB::commit();
            return response()->json([
                'message' => 'Data baru berhasil ditambahkan.',
                'serve' => [],
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function retrieve(Request $request)
    {
        try {
            $dataQuery = Product::where("id", $request->id)->first();
            if (!$dataQuery) {
                return response()->json([
                    'message' => 'Data tidak diketahui.',
                    'serve' => []
                ], 400);
            }

            return response()->json([
                'message' => '',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataQuery = Product::where('id', $request->id)->first();
            if (!$dataQuery) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }

            $dataQuery->name = $request->name;
            $dataQuery->stock = $request->stock;
            $dataQuery->price = $request->price;
            $dataQuery->price_disc = $request->price_disc;
            $dataQuery->price_total = $request->price - ($request->price * ($request->price_disc / 100));
            $dataQuery->save();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil diubah.',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function delete(Request $request)
    { 
        DB::beginTransaction();
        try {
            $dataQuery = Product::where('id', $request->id)->first();
            if (!$dataQuery) {
                DB::commit();
                return response()->json([
                    'message' => "Gagal mendapatkan data.",
                    'serve' => []
                ], 400);
            }
            $dataQuery->delete();
            DB::commit();
            return response()->json([
                'message' => 'Data berhasil dihapus.',
                'serve' => [],
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function getProductHome()
    {
        try {  
            $dataProductNew = Product::where('stock', '>', 0)
                                    ->orderBy('created_at', 'desc')
                                    ->limit(10)
                                    ->get();

            $dataProductPopular = Product::orderBy('name', 'asc')
                                    ->where("total_transaction", ">", 0)
                                    ->orderBy('total_transaction', 'desc')
                                    ->limit(10)
                                    ->get();
            return response()->json([
                'message' => '',
                'serve' => ["newest" => $dataProductNew, "populars" => $dataProductPopular],
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
