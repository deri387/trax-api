<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserBalanceHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserBalanceHistoryController extends Controller
{
    public function get(Request $request)
    {
        try {
            $type = $request->query("type");
            $dateStart = $request->query("date_start");
            $dateEnd = $request->query("date_end");
            $dataQuery = UserBalanceHistory::where("user_id", Auth::user()->id)
                                            ->when($type, function($query) use ($type){
                                                return $query->where('type', $type);
                                            })->when($dateStart && $dateEnd, function($query) use ($dateStart, $dateEnd){
                                                return $query->whereBetween('created_at', [$dateStart, $dateEnd]);
                                            })->orderBy("created_at", "desc")
                                            ->paginate(10);
            return response()->json([
                'message' => '',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
