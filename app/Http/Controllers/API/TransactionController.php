<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Support\Facades\Validator;
use App\Models\UserBalanceHistory;
use App\Utils\Helper;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function __construct()
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = env("MIDTRANS_SERVER_KEY");
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;
    }

    public function get(Request $request) {
        try {
            $date_start = $request->query("start_date");
            $date_end = $request->query("end_date");
            $limit = $request->query("limit");
            $dataQuery = Transaction::when($date_start && $date_end, function($query) use ($date_start, $date_end){
                return $query->whereBetween('created_at', [$date_start, $date_end]);
            })->orderBy("created_at", "desc")
            ->paginate($limit);
            return response()->json([
                'message' => '',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }

    public function topup(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'total' => 'required',
            ]);
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $total = $request->total;
            
            $dataUserBalance = new UserBalanceHistory();
            $dataUserBalance->user_id = Auth::user()->id;
            $dataUserBalance->type = 0;
            $dataUserBalance->total = $total;
            $dataUserBalance->save();

            $complete_request = [
                "transaction_details" => [
                    "order_id" => "TP".$dataUserBalance->id,
                    "gross_amount" => $dataUserBalance->total
                ],
            ];
            $snap_token = \Midtrans\Snap::getSnapToken($complete_request);

            $client = new Client();
            $client->post(env('APP_MIDDLEWARE')."/sendText", [
                'form_params' => [
                    'key' => env('OTP_KEY'),
                    'phone' => Auth::user()->phone,
                    'message' => "Telah terjadi top up saldo Trax sebesar ".Helper::formatRupiah($total).", silahkan selesaikan pembayaran Anda pada aplikasi.\n\n*Abaikan pesan ini jika Anda tidak melakukan proses top up.*",
                ]
            ]);
            DB::commit();
            return response()->json([
                'message' => '',
                'serve' => $snap_token,
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }

    public function transfer(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'to' => 'required',
                'total' => 'required',
            ]);
            $total = $request->total;
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataUserBalance = UserBalance::where("user_id", Auth::user()->id)->first();
            if (!$dataUserBalance) {
                DB::commit();
                return response()->json([
                    'message' => "Pengguna tidak diketahui.",
                    'serve' => []
                ], 400);
            }

            if ($dataUserBalance->balance < $total) {
                DB::commit();
                return response()->json([
                    'message' => "Saldo tidak mencukupi untuk transfer.",
                    'serve' => []
                ], 400);
            }
            
            $dataUserBalance->balance = $dataUserBalance->balance - $total;
            $dataUserBalance->save();

            $dataUserBalanceHistory = new UserBalanceHistory();
            $dataUserBalanceHistory->user_id = Auth::user()->id;
            $dataUserBalanceHistory->type = 1;
            $dataUserBalanceHistory->total = $total;
            $dataUserBalanceHistory->status = 1;
            $dataUserBalanceHistory->description = "Transfer ke ".$request->to;
            $dataUserBalanceHistory->save();

            $dataUserTransfer = UserBalance::where("wallet_address", $request->to)->first();
            if (!$dataUserTransfer) {
                DB::commit();
                return response()->json([
                    'message' => "Wallet address tidak diketahui.",
                    'serve' => []
                ], 400);
            }
            $dataUserTransfer->balance = $dataUserTransfer->balance + $total;
            $dataUserTransfer->save();

            $client = new Client();
            $client->post(env('APP_MIDDLEWARE')."/sendText", [
                'form_params' => [
                    'key' => env('OTP_KEY'),
                    'phone' => Auth::user()->phone,
                    'message' => "Telah terjadi transfer saldo Trax sebesar ".Helper::formatRupiah($total)." ke wallet address ".$request->to.", silahkan cek akun Anda pada aplikasi.\n\n*Abaikan pesan ini jika Anda tidak melakukan proses transfer.*",
                ]
            ]);

            $dataUserTransferPhone = User::where("id", $dataUserTransfer->user_id)->first();
            if (!$dataUserTransferPhone) {
                DB::commit();
                return response()->json([
                    'message' => "Wallet address tidak diketahui.",
                    'serve' => []
                ], 400);
            }
            $clientTransfer = new Client();
            $clientTransfer->post(env('APP_MIDDLEWARE')."/sendText", [
                'form_params' => [
                    'key' => env('OTP_KEY'),
                    'phone' => $dataUserTransferPhone->phone,
                    'message' => "Anda menerima saldo Trax sebesar ".Helper::formatRupiah($total)." dari wallet address ".$dataUserBalance->wallet_address.", silahkan cek akun Anda pada aplikasi.\n\n*Abaikan pesan ini jika Anda tidak menerima transfer.*",
                ]
            ]);
            DB::commit();
            return response()->json([
                'message' => 'Transfer sukses.',
                'serve' => [],
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }

    public function transaction(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'product_id' => 'required',
                'account' => 'required',
                'zone' => 'required',
                'nickname' => 'required',
            ]);
            $total = $request->total;
            $ref = "#".Helper::generateTransactionNumber();
            if ($validate->fails()) {
                DB::commit();
                return response()->json([
                    'message' => $validate->errors()->first(),
                    'serve' => []
                ], 400);
            }

            $dataUserBalance = UserBalance::where("user_id", Auth::user()->id)->first();
            if (!$dataUserBalance) {
                DB::commit();
                return response()->json([
                    'message' => "Pengguna tidak diketahui.",
                    'serve' => []
                ], 400);
            }

            if ($dataUserBalance->balance < $total) {
                DB::commit();
                return response()->json([
                    'message' => "Saldo tidak mencukupi untuk transaksi.",
                    'serve' => []
                ], 400);
            }

            $dataProduct = Product::where("id", $request->product_id)->first();
            if (!$dataProduct) {
                DB::commit();
                return response()->json([
                    'message' => "Produk tidak diketahui.",
                    'serve' => []
                ], 400);
            }

            $dataProduct->total_transaction = $dataProduct->total_transaction + 1;
            $dataProduct->save();
            
            $cashback = 2;
            if ($total > 100000 && $total < 500000) {
                $cashback = 5;
            }

            if ($total > 500000 && $total < 1000000) {
                $cashback = 10;
            }

            if ($total > 1000000) {
                $cashback = 15;
            }
            $dataUserBalance->balance = ($dataUserBalance->balance - $total) + ($total * ($cashback/100));
            $dataUserBalance->save();

            $dataUserBalanceHistory = new UserBalanceHistory();
            $dataUserBalanceHistory->user_id = Auth::user()->id;
            $dataUserBalanceHistory->type = 2;
            $dataUserBalanceHistory->total = $total;
            $dataUserBalanceHistory->status = 1;
            $dataUserBalanceHistory->description = "Pembayaran ".$ref;
            $dataUserBalanceHistory->save();

            $dataUserBalanceHistory = new UserBalanceHistory();
            $dataUserBalanceHistory->user_id = Auth::user()->id;
            $dataUserBalanceHistory->type = 3;
            $dataUserBalanceHistory->total = $total * ($cashback/100);
            $dataUserBalanceHistory->status = 1;
            $dataUserBalanceHistory->description = "Cashback ".$ref;
            $dataUserBalanceHistory->save();

            $dataTransaction = new Transaction();
            $dataTransaction->user_id = Auth::user()->id;
            $dataTransaction->product_id = $request->product_id;
            $dataTransaction->account = $request->account;
            $dataTransaction->zone = $request->zone;
            $dataTransaction->nickname = $request->nickname;
            $dataTransaction->total = $request->total;
            $dataTransaction->status = 1;
            $dataTransaction->save();

            $client = new Client();
            $client->post(env('APP_MIDDLEWARE')."/sendText", [
                'form_params' => [
                    'key' => env('OTP_KEY'),
                    'phone' => Auth::user()->phone,
                    'message' => "Telah terjadi transaksi dengan kode ref ".$ref." sebesar ".$total." sistem akan memproses transaksi Anda dalam beberapa menit ke depan. *Abaikan pesan ini jika Anda tidak melakukan transaksi*",
                ]
            ]);

            DB::commit();
            return response()->json([
                'message' => 'Transaksi sukses, kami akan memproses transaksi dalam beberapa menit ke depan.',
                'serve' => [],
            ], 200);
            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }

    public function webhookMidtrans(Request $request) {
        DB::beginTransaction();
        try {
            $type = substr($request->order_id, 0, 2);
            if ($type === "TP") {
                $id = substr($request->order_id, 2);
                $status = $request->transaction_status === "settlement" ? 1 : ($request->transaction === "pending" ? 0 : 2);

                $dataUserBalance = UserBalanceHistory::where("id", $id)->first();
                if (!$dataUserBalance) {
                    DB::commit();
                    return response()->json([
                        'message' => 'Transaksi tidak ditemukan.',
                        'serve' => [],
                    ], 200);
                }
                $dataUserBalance->status = $status;
                $dataUserBalance->save();
    
                if ($status === 1) {
                    $dataBalance = UserBalance::where("user_id", $dataUserBalance->user_id)->first();
                    if (!$dataBalance) {
                        DB::commit();
                        return response()->json([
                            'message' => 'Transaksi tidak ditemukan.',
                            'serve' => [],
                        ], 200);
                    }
                    $dataBalance->balance = $dataBalance->balance + $dataUserBalance->total;
                    $dataBalance->save();
                }

                $dataUser = User::where("id", $dataUserBalance->user_id)->first();
                if (!$dataUser) {
                    DB::commit();
                    return response()->json([
                        'message' => 'User tidak ditemukan.',
                        'serve' => [],
                    ], 200);
                }
                $client = new Client();
                $client->post(env('APP_MIDDLEWARE')."/sendText", [
                    'form_params' => [
                        'key' => env('OTP_KEY'),
                        'phone' => $dataUser->phone,
                        'message' => $status === 1 ? "Topup berhasil!\nTelah masuk saldo topup Trax ke akun Anda sebesar ".Helper::formatRupiah($dataUserBalance->total)."\n\n*Abaikan pesan ini jika Anda tidak melakukan proses topup.*" : "Top Up Masih Pending!\nTransaksi top up sebesar ".Helper::formatRupiah($dataUserBalance->total)." belum masuk ke saldo Trax dikarenakan pembayaran belum selesai.\n\n*Abaikan pesan ini jika Anda tidak melakukan proses topup.*"
                    ]
                ]);
            } 
            DB::commit();
            return response()->json([
                'message' => '',
                'serve' => [],
            ], 200);            
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => []
            ], 500);
        }
    }
}
