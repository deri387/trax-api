<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserBalance;
use Illuminate\Support\Facades\Auth;

class UserBalanceController extends Controller
{
    public function get()
    {
        try {
            $dataQuery = UserBalance::where("user_id", Auth::user()->id)->first();
            return response()->json([
                'message' => '',
                'serve' => $dataQuery,
            ], 200);
            
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'serve' => [],
            ], 500);
        }
    }
}
