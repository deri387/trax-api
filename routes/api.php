<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\UserBalanceController;
use App\Http\Controllers\API\UserBalanceHistoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("webhook", [TransactionController::class, "webhookMidtrans"]);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => '/v1/auth'], function () {
    Route::post('login', [AuthController::class, 'authenticated'])->name('login');
    Route::post('sendOtp', [AuthController::class, 'sendOtp'])->name('sendOtp');
    Route::post('login/root', [AuthController::class, 'authenticatedAdministrator'])->name('login.root');
});
Route::group(['middleware' => ['auth:api'], 'prefix' => 'v1'], function () {
    Route::get('balance', [UserBalanceController::class, 'get']);
    Route::get('logout', [AuthController::class, 'logout']);
    Route::put('password', [AuthController::class, 'updatePassword'])->name('updatePassword');
    /**
     * Route Product
     */
    Route::get('product', [ProductController::class, 'get']);
    Route::post('product', [ProductController::class, 'store']);
    Route::get('product/home', [ProductController::class, 'getProductHome']);
    Route::post('product/retrieve', [ProductController::class, 'retrieve']);
    Route::put('product', [ProductController::class, 'update']);
    Route::delete('product', [ProductController::class, 'delete']);
    /**
     * 
     * Route Transaction
     */
    Route::get('transaction', [TransactionController::class, 'get']);
    Route::post('topup', [TransactionController::class, 'topup']);
    Route::post('transfer', [TransactionController::class, 'transfer']);
    Route::post('transaction', [TransactionController::class, 'transaction']);
    Route::get('history', [UserBalanceHistoryController::class, 'get']);
});